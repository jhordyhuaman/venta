import os
from datetime import datetime
from dateutil import parser as datetime_parser
from dateutil.tz import tzutc
from flask import Flask, url_for, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from .utils import split_url

app = Flask(__name__)
app_settings = os.getenv('APP_SETTINGS')
app.config.from_object(app_settings)

db = SQLAlchemy(app)

class ValidationError(ValueError):
    pass

class Factura(db.Model):
    __tablename__='facturas'
    id = db.Column(db.Integer, primary_key=True)
    fecha = db.Column(db.DateTime, default=datetime.now)
    id_cliente = db.Column(db.Integer,db.ForeignKey('clientes.id'))
    detalle = db.relationship('Detalle',backref='detalle', lazy='dynamic')

    def get_url(self):
        return url_for('get_factura', id=self.id, _external=True)

    def export_data(self):
        return {
            'self_url': self.get_url(),
            'fecha': self.fecha
        }

    def import_data(self, data):
        try:
            self.fecha = datetime_parser.parse(data['fecha']).astimezone(
                tzutc()).replace(tzinfo=None)
        except KeyError as e:
            raise ValidationError('Invalid factura: missing ' + e.args[0])
        return self

class Cliente(db.Model):
    __tablename__ = 'clientes'
    id = db.Column(db.Integer, primary_key=True)
    factura = db.relationship('Factura',backref='factura', lazy='dynamic')
    nombre = db.Column(db.String(64), index=True)
    apellido = db.Column(db.String(64), index=True)
    email = db.Column(db.String(64), index=True)
    direccion = db.Column(db.String(64), index=True)

    def get_url(self):
        return url_for('get_cliente', id=self.id, _external=True)
    
    def export_data(self):
        return {
            'self_url': self.get_url(),
            'nombre': self.nombre,
            'apellido': self.apellido,
            'email': self.email,
            'direccion': self.direccion,
        }

    def import_data(self, data):
        try:
            self.nombre = data['nombre']
            self.apellido = data['apellido']
            self.email = data['email']
            self.direccion = data['direccion']
        except KeyError as e:
            raise ValidationError('Invalid factura: missing ' + e.args[0])
        return self
    
class Detalle(db.Model):
    __tablename__='detalles'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(64),index=True)
    precio = db.Column(db.String(64),index=True)
    id_factura = db.Column(db.Integer,db.ForeignKey('facturas.id'),index=True)
    id_producto = db.Column(db.Integer,db.ForeignKey('productos.id'))
    
class Producto(db.Model):
    __tablename__='productos'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(64),index=True)
    stock = db.Column(db.String(64),index=True)
    detalle = db.relationship('Detalle',backref='detalle_prod', lazy='dynamic')

    def get_url(self):
        return url_for('get_producto', id=self.id, _external=True)

    def export_data(self):
        return {
            'self_url': self.get_url(),
            'nombre': self.nombre,
            'stock': self.stock,
        }

    def import_data(self, data):
        try:
            self.nombre = data['nombre']
            self.stock = data['stock']
        except KeyError as e:
            raise ValidationError('Invalid factura: missing ' + e.args[0])
        return self


#################### Routes ####################
# Rutas Facturas
@app.route('/facturas/', methods=['GET'])
def get_facturas():
    return jsonify({
        'factura': [
            factura.get_url() 
            for factura in
            Factura.query.all()
        ]
    })
@app.route('/facturas/<int:id>', methods=['GET'])
def get_factura(id):
    return jsonify(Factura.query.get_or_404(id).export_data())
@app.route('/facturas/', methods=['POST'])
def new_factura():
    factura = Factura()
    factura.import_data(request.json)
    db.session.add(factura)
    db.session.commit()
    return jsonify({}), 201, {'Location': factura.get_url()}
# Rutas Cliente
@app.route('/clientes/', methods=['GET'])
def get_clientes():
    return jsonify({
        'clientes': [
            cliente.get_url() 
            for cliente in
            Cliente.query.all()
        ]
    })
@app.route('/clientes/<int:id>', methods=['GET'])
def get_cliente(id):
    return jsonify(Cliente.query.get_or_404(id).export_data())
@app.route('/clientes/', methods=['POST'])
def new_cliente():
    cliente = Cliente()
    cliente.import_data(request.json)
    db.session.add(cliente)
    db.session.commit()
    return jsonify({}), 201, {'Location': cliente.get_url()}
# Rutas Productos
@app.route('/productos/', methods=['GET'])
def get_productos():
        return jsonify({
        'productos': [
            producto.get_url() 
            for producto in
            Producto.query.all()
        ]
    })
@app.route('/productos/<int:id>', methods=['GET'])
def get_producto(id):
    return jsonify(Producto.query.get_or_404(id).export_data())
@app.route('/productos/', methods=['POST'])
def new_producto():
    producto = Producto()
    producto.import_data(request.json)
    db.session.add(producto)
    db.session.commit()
    return jsonify({}), 201, {'Location': producto.get_url()}


if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)